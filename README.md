# Invoke Automation Tasks

This repository contains code examples used in my blog post on how to streamline development workflows with Python's Invoke. The post covers practical uses of Invoke for task automation, focusing on serverless operations and backend management.

[Read the Blog Post](https://medium.com/p/1ff09f645b4c)

## Getting Started

To get started with these examples, you'll need to have Python installed on your system. If you don't have Python installed, [download and install it from python.org](https://www.python.org/downloads/). This code is tested with Python 3.8+.

Once Python is set up, follow these steps:

### Installation

1. Clone the repository:

    ```sh
    git clone https://github.com/your-username/your-repo-name.git
    cd your-repo-name
    ```

2. It's recommended to use a virtual environment to keep dependencies contained:

    ```sh
    python -m venv venv
    source venv/bin/activate  # On Windows use `venv\Scripts\activate`
    ```

3. Install the required packages:
    ```sh
    pip install -r requirements.txt
    ```

### Using the Invoke Tasks

To list all available tasks, run:

```sh
invoke --list
```

To execute a specific task, use:

```sh
invoke <task-name>
```

For example, to add a user with Invoke:

```sh
invoke user.add --username="johndoe"
```

To see detailed help for a specific task, run:

```sh
invoke --help <task-name>
```

## Structure

The repository is structured as follows:

-   `tasks/`: Contains Invoke task definitions.
    -   `aws.py`: Tasks related to AWS operations.
    -   `users.py`: Tasks for user management.

## Contributing

Contributions are what make the open-source community such an amazing place to learn, inspire, and create. Any contributions you make are **greatly appreciated**.

If you have a suggestion that would improve this, please fork the repo and create a pull request. You can also simply open an issue with the tag "enhancement".

Don't forget to give the project a star! Thanks again!

## License

Distributed under the MIT License. See `LICENSE` for more information.

## Contact

Your Name - [@AxeMindx](https://twitter.com/AxeMindx)

Project Link: https://gitlab.com/medium-samples/invoke-sample/

```

```
