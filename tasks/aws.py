"""aws-cli tasks."""
from invoke import task

@task
def add(ctx, key, value):
    print(f"Adding {key} to SSM with value: {value}.")

@task
def ssm_parameter_read(ctx):
    print("Fetching value from SSM.")

