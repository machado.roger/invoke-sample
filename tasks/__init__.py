"""Invoke module."""
from invoke import Collection

from . import aws, users

ns = Collection(aws, users)
