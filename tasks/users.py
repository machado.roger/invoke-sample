"""users-cli tasks."""
from invoke import task

@task
def add(ctx, username):
    print(f"Adding new user: {username}")

@task
def list(ctx):
    print("Listing all users...")

